**INTRODUCTION:**

**Developing an online web application that can act as portal for money
transfer. In this we can transfer and receive money. And we can
communicate easily with multiple banks across the world.**

**In this application mainly consists of 1 actor namely customer. The
customers side consists of the sign-up and sign-in pages. In the in
sign-up page, it has components like name, mobile number, email,
password, KYC process. And in sign-in page consists of username and
password.**

**BASIC OPERATIONS:**

1.  **Customers have to create an account, then they can access the Net
    banking application.**

2.  **KYC process is done by admin page**

3.  **In account should have minimum balance with 1000**

4.  **In this net banking, the customers can transfer and receive the
    money.**

5.  **Anyone can take loan in this net banking according their cibil
    score, if the cibil score above 700.**

6.  **Bill payments like loan, credit card.**

7.  **In statement, we can check our transactions.**
